import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { ManagerService } from './manager.service';
import { CreateCovidDTO } from './dto/create.covid.dto';

@Controller('manager')
export class ManagerController {
    constructor(private managerService: ManagerService) { }
    @Get('test')
    async test(){
        return await this.managerService.test()
    }
    @Post('create_covid')
    create_covid(@Body() data: CreateCovidDTO) {
        return this.managerService.CreateCovid(data);
    }
    @Get('data_covid')
    get_dataCovid() {
        return this.managerService.GetDataCovid();
    }
    @Get('data_covid/:user_id')
    get_dataCovidByID(@Param('user_id') user_id:string) {
        return this.managerService.GetDataCovidByID(user_id);
    }

    @Patch('update/data_covid/:user_id')
    update_data_covid(@Param('user_id') user_id:string ,@Body() dataUpdate:CreateCovidDTO){
        return this.managerService.UpdateDataCovid(user_id,dataUpdate)
    }

    @Delete('delete/data_covid/:user_id')
    delete_data_covid(@Param('user_id') user_id:string){
        return this.managerService.DeleteDataCovid(user_id)
    }
}


