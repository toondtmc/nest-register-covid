import { Injectable } from '@nestjs/common';
import { DbService } from '../db/db.service';
import { CreateCovidDTO } from './dto/create.covid.dto';


@Injectable()
export class ManagerService {
    constructor(
        private dbService: DbService,
    ) {}

    async test(){
        return"toon"
    }
    async CreateCovid(dataDTO:CreateCovidDTO){
        return await this.dbService.createDataCovid(dataDTO)
    }
   async GetDataCovid(){
    return await this.dbService.getDataCovid()
   }
   async GetDataCovidByID(id:string){
    return await this.dbService.getDataCovidByID({user_id: id})
   }

   async UpdateDataCovid(user_id:string , dataUpdate:CreateCovidDTO){
    return await this.dbService.updateDataCovid(user_id,dataUpdate)
   }
   async DeleteDataCovid(user_id:string){
    return await this.dbService.deleteDataCovid(user_id)
   }
   
}
