import { Module } from '@nestjs/common';
import { DbModule } from '../db/db.module';
import { ManagerService } from './manager.service';
import { ManagerController } from './manager.controller';

@Module({
    imports: [DbModule],
    providers: [ManagerService],
    controllers: [ManagerController]
})
export class MangerModule {}
