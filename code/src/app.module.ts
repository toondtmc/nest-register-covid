import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ManagerController } from './manager/manager.controller';
import { ManagerService } from './manager/manager.service';
import { MangerModule } from './manager/manager.module';
import { DbModule } from './db/db.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forRoot(
      'mongodb://localhost:27017',
          {
              dbName: 'covid_19',
          },
    ),
    MangerModule, DbModule],
  controllers: [AppController, ManagerController],
  providers: [AppService, ManagerService],
})
export class AppModule {}
