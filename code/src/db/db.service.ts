import { BadGatewayException, HttpCode, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { createCovidCollection } from './schema/create.covid.schema';
import { CreateCovidDTO } from 'src/manager/dto/create.covid.dto';
import * as moment from 'moment';

@Injectable()
export class DbService {
    constructor(
        @InjectModel(createCovidCollection.name)
        private createModel: Model<createCovidCollection>,
        
    ){}
    async createDataCovid(dataDTO:CreateCovidDTO){
      const  cre = new this.createModel(dataDTO)
    //   console.log(cre)
      return await cre.save();
    }
    async getDataCovid(){
        try {
            const res = await this.createModel.find().exec();
            return res;
        } catch (err) {
            throw new HttpException(err.message, HttpStatus.BAD_GATEWAY);
        }
    }

    async getDataCovidByID(q: any){
        try {
            return await this.createModel.findOne(q).exec()
        } catch (err) {
            throw new HttpException(err.message, 500);
        }
    }
    async updateDataCovid(user_id:string , updateData:CreateCovidDTO){
        try{
        const nowUinx = moment().unix()
        updateData.updated_at = nowUinx
        
        const result = await this.createModel.findOneAndUpdate({user_id},updateData).exec();
        console.log(updateData)
        
        return "updated successfully"
      
      } catch (error) {
      
        throw new HttpException(`Could not update data covid: ${error.message}`,  HttpStatus.BAD_REQUEST);
        
      }
    }

      async deleteDataCovid(user_id:string){
        try{
            const res = await this.createModel.deleteOne({user_id:user_id})
            return "deleted successfully"
        }catch(error){
            throw new HttpException(`Could not delete : ${error.message}`,  HttpStatus.BAD_REQUEST);
        }
        
      }
    
}
