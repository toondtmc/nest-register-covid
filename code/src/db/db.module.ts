import { Module } from '@nestjs/common';
import { DbService } from './db.service';
import { MongooseModule } from '@nestjs/mongoose';
import { creatCovidSchema, createCovidCollection } from './schema/create.covid.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
          name: createCovidCollection.name,
          schema: creatCovidSchema,
      },
      
  ]),
],
providers: [DbService],
exports: [DbService],
})
export class DbModule {}
