import { Prop } from "@nestjs/mongoose"
import * as moment from 'moment';
import { IsBoolean, IsNumber,  IsString,IsEmail, IsArray, IsObject, IsOptional } from "class-validator";
import { uuidNoDash } from "../share";

export class Itinerary{
  @Prop()
  @IsString()
  domestic_or_abroad:string
  @Prop()
  @IsString()
  province_or_country:string
  @Prop()
  @IsNumber()
  departure_date:number
  @Prop()
  @IsNumber()
  arrival_date:number
}

export class Contact_person{
  @Prop()
  @IsString()
  frist_name_contact:string
  @Prop()
  @IsString()
  last_name_contact:string
  @Prop()
  @IsString()
  phone_number_contact:number
  @Prop()
  @IsString()
  relationship:string
}

export class Info_about_infection{
  @Prop()
  @IsNumber()
  date_of_inspection: number
  @Prop()
  @IsBoolean()
  result:boolean
  @Prop()
  @IsArray()
  Symptom: string[]
  @Prop()
  @IsObject()
  itinerary:Itinerary
}

export class Register_Covid_19{
  @Prop({default: () => uuidNoDash()})
  user_id: string
  @Prop()
  @IsString()
  first_name : string
  @Prop()
  @IsString()
  last_name :string
  @Prop()
  @IsNumber()
  age : number
  @Prop()
  @IsString()
  sex:string
  @Prop()
  @IsBoolean()
  vaccination:boolean
  @Prop()
  @IsString()
  phone_number:string
  @Prop()
  @IsEmail()
  email:string
  @Prop()
  @IsString()
  address:string
  @Prop()
  @IsObject()
  contact_person:Contact_person
  @Prop()
  @IsObject()
  info_about_infection : Info_about_infection
@Prop({default:()=>moment().unix()})
@IsNumber()
@IsOptional()
created_at:number
@Prop()
@IsNumber()
@IsOptional()
updated_at: number
}