import { HttpException } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';


export const uuidNoDash = () => {
    return uuidv4().replace(/-/g, '');
};
